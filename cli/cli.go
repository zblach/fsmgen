package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"github.com/urfave/cli/v2"

	"github.com/zblach/fsmgen/v2/graph"
	"github.com/zblach/fsmgen/v2/templates/basic"
)

func main() {
	basic_error_options := map[string]basic.ErrorMode{
		"recover":   basic.Recover,
		"blackhole": basic.Blackhole,
		"cascade":   basic.Cascade,

		"r": basic.Recover,
		"b": basic.Blackhole,
		"c": basic.Cascade,
	}

	app := &cli.App{
		Commands: []*cli.Command{
			{
				Name:    "generate",
				Aliases: []string{"g", "gen"},
				Usage:   "Generate a state machine",
				Subcommands: []*cli.Command{
					{
						Name:    "basic",
						Aliases: []string{"b"},
						Usage:   "Generate a basic state machine",
						Action: func(c *cli.Context) error {
							errType := strings.ToLower(c.String("err"))
							t, err := basic.New(basic.Options{
								ErrorMode:                 basic_error_options[errType],
								IncludeGraphvizErrorState: c.Bool("doterr"),
								IncludeSubgraphs:          c.Bool("sg"),
							})
							if err != nil {
								return err
							}

							g, err := graph.FromFile(c.Args().Get(0))
							if err != nil {
								return err

							}
							g.Context = graph.Context{
								Command:   os.Args,
								GoModRoot: "play.ground",
							}

							o, err := t.Render(*g)
							if err != nil {
								return err
							}

							if c.String("o") == "-" {
								bf, err := o.ToPlayground()
								if err != nil {
									return err
								}
								bs, err := io.ReadAll(bf)
								if err != nil {
									return err
								}
								os.Stdout.Write(bs)
							} else {
								if err := o.ToFiles(c.String("o")); err != nil {
									return err
								}
							}

							return nil
						},
						Flags: []cli.Flag{
							&cli.StringFlag{
								Name:        "error handling",
								Aliases:     []string{"e", "err"},
								Usage:       "Error handling strategy",
								Required:    false,
								DefaultText: `"recover" -> [ "recover" | "blackhole" | "cascade" ]`,
								Value:       "recover",
								Action: func(c *cli.Context, param string) error {
									if _, ok := basic_error_options[strings.ToLower(param)]; !ok {
										return fmt.Errorf("unknown error handling strategy: %q", param)
									}
									return nil
								},
							},
							&cli.PathFlag{
								Name:     "output directory",
								Aliases:  []string{"o"},
								Usage:    "Output directory",
								Required: false,
								Value:    ".",
							},
							&cli.BoolFlag{
								Name:     "subgraphs",
								Aliases:  []string{"s", "sg", "subg"},
								Usage:    "Generate subgraph interfaces",
								Value:    false,
								Required: false,
							},
							&cli.BoolFlag{
								Name:     "graphviz error state",
								Aliases:  []string{"doterr"},
								Usage:    "Generate a error state in the graphviz output",
								Value:    false,
								Required: false,
							},
						},
					},
				},
			},
		},
	}

	app.Suggest = true

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
