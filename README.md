# fsmgen

`fsmgen` is a tool that converts a graphviz dotfile into a series of interlocking interfaces with zero-cost storage abstactions.

These interfaces make it impossible to perform an unexpected state transition, as undefined edges are _syntactically illegal_ and chains involving cannot be resolved. 

## why

A major source of application bugs stems from an invalid or unexpected state change. By codifying the state machine, and only implementing functions for defined edges, we can tell a cleaner story about the state of an application.

Consider ![](examples/digraphs/G.svg), and the generated module at `fixtures/basic/recover/no_gv_error_state/subgraph/G`. It has a defined `start` and `end` state, and two separate subgraphs.

Arbitrarily beginning from `g.Start()`, we can chain calls to any other state, provided there's a legal edge at every step along the way between them. 


## how to use
`fsmgen generate basic -o <dir> --sg path/to/definition.dot`

This will generate a standalone module named for the main graph defined in the `.dot` file. This tool will generate the state machine, expose functions and interfaces that allow one to reason/interact with the machine, and skeletons for every edge in the graph.

### options

Currently, only `basic` digraphs are supported. I'm still playing with patterns I like for application state transferrence between edges, and edge-name or function/symbol extraction.

#### error states
- recovery

This mode generates an additional node called `_err` that gets routed to if any of the edge functions fail. It also generates skeleton functions to recover from this error state and back into any edge.

- blackhole

This mode generates a static, inescapable error state.

- cascade

This mode continues to coalesce error states after other failed transitions.

- panic

Do away with error states entirely, and just `panic` if a failure occurs during a state transition. Make sure to `recover()` in your code.