package graph

import (
	"errors"
	"fmt"
	"go/token"
	"os"
	"strings"

	"github.com/awalterschulze/gographviz"
)

var (
	ErrNoGraph               = errors.New("no graph")
	ErrUndirectedUnsupported = errors.New("undirected graphs are not currently supported")
	ErrReservedWord          = errors.New("state name is a reserved word")
	ErrDuplicateState        = errors.New("duplicate state")
)

type AddErrorStateFunction func(*gographviz.Graph) (*gographviz.Graph, error)

type Context struct {
	// Invocation Command
	Command   []string
	GoModRoot string
}

type Graph struct {
	*gographviz.Graph

	Context
}

func New(graph *gographviz.Graph) (*Graph, error) {
	if graph == nil {
		return nil, ErrNoGraph
	}
	if !graph.Directed {
		return nil, ErrUndirectedUnsupported
	}
	// check for invalid/duplicate/case-ambiguous states
	s := map[string]struct{}{}
	for _, n := range graph.Nodes.Nodes {
		lower := strings.ToLower(n.Name)

		if token.IsKeyword(lower) {
			return nil, fmt.Errorf("%w: %s", ErrReservedWord, lower)
		}
		if _, ok := s[lower]; ok {
			return nil, fmt.Errorf("%w: %s", ErrDuplicateState, lower)
		}

		s[lower] = struct{}{}
	}
	return &Graph{
		Graph: graph,
		Context: Context{
			Command:   []string{},
			GoModRoot: "play.ground",
		}}, nil
}

func FromFile(filename string) (*Graph, error) {
	b, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	g, err := gographviz.Read(b)
	if err != nil {
		return nil, err
	}

	return New(g)
}
