package common

import (
	"bytes"
	"go/format"
	"path"
	"strings"
	"text/template"

	"github.com/zblach/fsmgen/v2/graph"
)

type Template struct {
	template.Template
	Files map[string]string
}

// Render renders the template with the given graph and returns the output.
// The output contains the rendered files.
func (t Template) Render(g graph.Graph) (*Output, error) {
	ret := &Output{
		Graph: &g,
		Files: map[string][]byte{},
	}

	for name, def := range t.Files {
		var b bytes.Buffer
		switch strings.ToLower(path.Ext(name)) {
		case ".go":
			if err := t.Template.ExecuteTemplate(&b, def, g); err != nil {
				return nil, err
			}
			src, err := format.Source(b.Bytes())
			if err != nil {
				return nil, err
			}
			b = *bytes.NewBuffer(src)

		default:
			if err := t.Template.ExecuteTemplate(&b, def, g); err != nil {
				return nil, err
			}
		}

		ret.Files[name] = b.Bytes()
	}
	return ret, nil
}
