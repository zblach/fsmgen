package common

import (
	"errors"
	"fmt"
	"go/token"
	"strings"
	"text/template"
	"time"

	"github.com/awalterschulze/gographviz"
	"github.com/iancoleman/strcase"
)

type (
	fcn   func(string) string
	cache map[string]string
)

type memoized struct {
	fcn
	cache
	hits uint
}

func memoize(f fcn) func(string) (string, error) {
	m := &memoized{
		fcn:   f,
		cache: cache{},
	}
	return m.exec
}

func (m *memoized) exec(str string) (string, error) {
	if v, ok := m.cache[str]; ok {
		m.hits++
		return v, nil
	}
	v := m.fcn(str)
	if !token.IsIdentifier(v) {
		return "", fmt.Errorf("'%s' is not a valid identifier, and cannot be used", v)
	}
	m.cache[str] = v
	return v, nil
}

func Funcs() template.FuncMap {
	return template.FuncMap{
		// Text functions
		"pkg": memoize(strcase.ToSnake),

		// TODO: make configurable
		"export": memoize(strcase.ToCamel),
		"struct": memoize(func(s string) string { return strings.ToLower(s) }),
		"name":   memoize(func(s string) string { return s }),

		// Utility functions
		"now":     func() string { return time.Now().Format(time.RFC1123) },
		"strjoin": func(strs []string) string { return strings.Join(strs, " ") },
		"dict": func(values ...interface{}) (map[string]interface{}, error) {
			if len(values)%2 != 0 {
				return nil, errors.New("invalid dict call")
			}
			dict := make(map[string]interface{}, len(values)/2)
			for i := 0; i < len(values); i += 2 {
				key, ok := values[i].(string)
				if !ok {
					return nil, errors.New("dict keys must be strings")
				}
				dict[key] = values[i+1]
			}
			return dict, nil
		},

		// Graph functions
		"randof": func(m map[string][]*gographviz.Edge) []*gographviz.Edge {
			var v []*gographviz.Edge
			for _, v = range m {
				break
			}
			return v
		},
		"graphmod": func(g *gographviz.Graph) (*gographviz.Graph, error) {
			return g, nil
		},
	}
}
