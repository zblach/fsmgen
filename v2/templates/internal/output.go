package common

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"github.com/iancoleman/strcase"
	"github.com/zblach/fsmgen/v2/graph"
)

type Output struct {
	*graph.Graph
	Files map[string][]byte
}

func replacer(reps ...string) *strings.Replacer {
	// default replacements
	rep := map[string]string{
		"{now}": fmt.Sprintf("%d", time.Now().Unix()),
	}

	for i := 0; i < len(reps); i += 2 {
		rep[reps[i]] = reps[i+1]
	}
	rs := make([]string, 0, len(rep)*2)
	for k, v := range rep {
		rs = append(rs, k, v)
	}
	return strings.NewReplacer(rs...)
}

func toFilename(filename, modulename string, replacer *strings.Replacer) string {
	return filepath.Clean(path.Join(strcase.ToSnake(modulename), replacer.Replace(filename)))
}

func (o *Output) ToFiles(outdir string, reps ...string) (err error) {
	replacer := replacer(reps...)

	for filename, data := range o.Files {
		fullpath := path.Join(outdir, toFilename(filename, o.Name, replacer))
		if err = os.MkdirAll(path.Dir(fullpath), 0755); err != nil {
			return
		}
		if err = os.WriteFile(fullpath, data, 0644); err != nil {
			return
		}
	}

	return nil
}

func (o *Output) ToPlayground(reps ...string) (io.Reader, error) {
	replacer := replacer(reps...)

	var buf bytes.Buffer

	for filename, data := range o.Files {
		if _, err := fmt.Fprintf(&buf, "-- %s --\r\n", toFilename(filename, o.Name, replacer)); err != nil {
			return nil, err
		}
		if _, err := buf.Write(data); err != nil {
			return nil, err
		}
	}

	return bytes.NewReader(buf.Bytes()), nil
}
