package templates_test

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/zblach/fsmgen/v2/graph"
	"github.com/zblach/fsmgen/v2/templates/basic"
	common "github.com/zblach/fsmgen/v2/templates/internal"
)

const examplesDir = "../../examples/digraphs"

func RegenerateFixtures() error {
	files, err := filepath.Glob(path.Join(examplesDir, "*.dot"))
	if err != nil {
		return err
	}
	templates := map[string]*common.Template{}

	// prepare all template combinations
	for ets, etype := range map[string]basic.ErrorMode{
		"recover":   basic.Recover,
		"blackhole": basic.Blackhole,
		"cascade":   basic.Cascade,
		"panic":     basic.Panic,
	} {
		for ges, includeGraphvizErrorState := range map[string]bool{
			"with_gv_error_state": true,
			"no_gv_error_state":   false,
		} {
			for sgs, includeSubgraphs := range map[string]bool{
				"subgraph":    true,
				"no_subgraph": false,
			} {
				templates[fmt.Sprintf("basic/%s/%s/%s", ets, ges, sgs)], err = basic.New(
					basic.Options{
						ErrorMode:                 etype,
						IncludeGraphvizErrorState: includeGraphvizErrorState,
						IncludeSubgraphs:          includeSubgraphs,
					})
				if err != nil {
					return err
				}
			}
		}
	}

	// generate all graphs in all templates
	for _, file := range files {
		for dir, template := range templates {
			g, err := graph.FromFile(file)
			if err != nil {
				return err
			}
			g.Context.GoModRoot = fmt.Sprintf("%s/%s", dir, g.Name)
			o, err := template.Render(*g)
			if err != nil {
				return err
			}
			// Write module to directory
			if err := o.ToFiles(path.Join("../../fixtures", dir, g.Name), "{now}", "fixture"); err != nil {
				return err
			}
			// Write playground file
			br, err := o.ToPlayground("{now}", "fixture")
			if err != nil {
				return err
			}
			bs, err := ioutil.ReadAll(br)
			if err != nil {
				return err
			}
			if err := os.WriteFile(path.Join("../../fixtures", dir, g.Name, "playground.txt"), bs, 0644); err != nil {
				return err
			}
		}
	}

	return nil
}
func TestRegenerateFixtures(t *testing.T) {
	assert.NoError(t, RegenerateFixtures())
	// don't forget to run 'go work use -r fixtures/' to use the new fixtures
}
