package basic

import (
	"errors"
	"text/template"

	common "github.com/zblach/fsmgen/v2/templates/internal"
)

type ErrorMode int

const (
	Recover = iota
	Blackhole
	Cascade
	Panic
)

var (
	ErrUnknownErrorMode = errors.New("unknown error mode")
)

type Options struct {
	ErrorMode
	IncludeGraphvizErrorState bool
	IncludeSubgraphs          bool
}

func New(options Options) (*common.Template, error) {
	template := &common.Template{
		Template: *template.New("basic"),
		Files: map[string]string{
			"../go.mod":                 go_mod,
			"../example.go":             example,
			"states.go":                 states,
			"transitions.go":            transitions,
			"transitions_impl_{now}.go": transition_implementations,
			"graph.dot":                 dotfile,
		},
	}

	if options.IncludeSubgraphs {
		template.Files["subgraphs.go"] = subgraphs
	}

	funcs := common.Funcs()

	// add common definitions
	common_defs := map[string]string{
		"preamble":     preamble,
		"modify":       modify,
		"nomodify":     nomodify,
		"regenerate":   regenerate,
		"error_return": error_return,
	}

	// add error mode to files generated
	switch options.ErrorMode {
	case Recover:
		template.Files["error_state_{now}.go"] = recovery_error_state
		if options.IncludeGraphvizErrorState {
			funcs["graphmod"] = AddRecoveryErrorState
		}
	case Blackhole:
		template.Files["error_state.go"] = blackhole_error_state
		if options.IncludeGraphvizErrorState {
			funcs["graphmod"] = AddBlackholeErrorState
		}
	case Cascade:
		template.Files["error_state.go"] = cascade_error_state
		if options.IncludeGraphvizErrorState {
			funcs["graphmod"] = AddCascadingErrorState
		}
	case Panic:
		common_defs["error_return"] = panic_error
		if options.IncludeGraphvizErrorState {
			funcs["graphmod"] = AddBlackholeErrorState
		}
	default:
		return nil, ErrUnknownErrorMode
	}

	template.Funcs(funcs)

	for def, content := range common_defs {
		_, err := template.New(def).Parse(content)
		if err != nil {
			return nil, err
		}
	}

	for filename, definition := range template.Files {
		_, err := template.New(filename).Parse(definition)
		if err != nil {
			return nil, err
		}
		template.Files[filename] = filename
	}

	return template, nil
}
