// file automatically generated by fsmgen at Tue, 04 Apr 2023 20:47:39 PDT
// modify or implement the following functions to customize the generated code
//
//go:generate
package shitpost

import (
	"fmt"
)

var (
	ErrFailTransition = fmt.Errorf("failed transition")
)

// p0 → p1
func (p0) P1() maybeP1 {
	// TODO: implement
	if false {
		// Error case
		return _err{error: ErrFailTransition, from: p0{}, to: p1{}}
	}
	return p1{}
}

// p0 → p2
func (p0) P2() maybeP2 {
	// TODO: implement
	if false {
		// Error case
		return _err{error: ErrFailTransition, from: p0{}, to: p2{}}
	}
	return p2{}
}

// p1 → p1
func (p1) P1() maybeP1 {
	// TODO: implement
	if false {
		// Error case
		return _err{error: ErrFailTransition, from: p1{}, to: p1{}}
	}
	return p1{}
}

// p1 → p2
func (p1) P2() maybeP2 {
	// TODO: implement
	if false {
		// Error case
		return _err{error: ErrFailTransition, from: p1{}, to: p2{}}
	}
	return p2{}
}

// p1 → p3
func (p1) P3() maybeP3 {
	// TODO: implement
	if false {
		// Error case
		return _err{error: ErrFailTransition, from: p1{}, to: p3{}}
	}
	return p3{}
}

// p2 → p0
func (p2) P0() maybeP0 {
	// TODO: implement
	if false {
		// Error case
		return _err{error: ErrFailTransition, from: p2{}, to: p0{}}
	}
	return p0{}
}

// p2 → p3
func (p2) P3() maybeP3 {
	// TODO: implement
	if false {
		// Error case
		return _err{error: ErrFailTransition, from: p2{}, to: p3{}}
	}
	return p3{}
}

// p2 → p4
func (p2) P4() maybeP4 {
	// TODO: implement
	if false {
		// Error case
		return _err{error: ErrFailTransition, from: p2{}, to: p4{}}
	}
	return p4{}
}

// p3 → p3
func (p3) P3() maybeP3 {
	// TODO: implement
	if false {
		// Error case
		return _err{error: ErrFailTransition, from: p3{}, to: p3{}}
	}
	return p3{}
}

// p3 → p4
func (p3) P4() maybeP4 {
	// TODO: implement
	if false {
		// Error case
		return _err{error: ErrFailTransition, from: p3{}, to: p4{}}
	}
	return p4{}
}

// p4 → p5
func (p4) P5() maybeP5 {
	// TODO: implement
	if false {
		// Error case
		return _err{error: ErrFailTransition, from: p4{}, to: p5{}}
	}
	return p5{}
}

// p5 → p2
func (p5) P2() maybeP2 {
	// TODO: implement
	if false {
		// Error case
		return _err{error: ErrFailTransition, from: p5{}, to: p2{}}
	}
	return p2{}
}

// p5 → p3
func (p5) P3() maybeP3 {
	// TODO: implement
	if false {
		// Error case
		return _err{error: ErrFailTransition, from: p5{}, to: p3{}}
	}
	return p3{}
}

// p5 → p5
func (p5) P5() maybeP5 {
	// TODO: implement
	if false {
		// Error case
		return _err{error: ErrFailTransition, from: p5{}, to: p5{}}
	}
	return p5{}
}
