// file automatically generated by fsmgen at Tue, 04 Apr 2023 20:47:39 PDT
// modify or implement the following functions to customize the generated code
//
//go:generate
package disconnected

import (
	"fmt"
)

var (
	ErrFailTransition = fmt.Errorf("failed transition")
)

// A → B
func (a) B() maybeB {
	// TODO: implement
	if false {
		// Error case
		panic(fmt.Errorf("%w: A → B", ErrFailTransition))
	}
	return b{}
}

// B → C
func (b) C() maybeC {
	// TODO: implement
	if false {
		// Error case
		panic(fmt.Errorf("%w: B → C", ErrFailTransition))
	}
	return c{}
}

// C → A
func (c) A() maybeA {
	// TODO: implement
	if false {
		// Error case
		panic(fmt.Errorf("%w: C → A", ErrFailTransition))
	}
	return a{}
}

// D → E
func (d) E() maybeE {
	// TODO: implement
	if false {
		// Error case
		panic(fmt.Errorf("%w: D → E", ErrFailTransition))
	}
	return e{}
}

// E → F
func (e) F() maybeF {
	// TODO: implement
	if false {
		// Error case
		panic(fmt.Errorf("%w: E → F", ErrFailTransition))
	}
	return f{}
}

// F → D
func (f) D() maybeD {
	// TODO: implement
	if false {
		// Error case
		panic(fmt.Errorf("%w: F → D", ErrFailTransition))
	}
	return d{}
}
