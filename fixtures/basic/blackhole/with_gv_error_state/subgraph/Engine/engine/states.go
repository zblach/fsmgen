// file automatically generated by fsmgen at Tue, 04 Apr 2023 20:47:39 PDT
// do not modify the following definitions
//
//vim:ro
//go:generate
package engine

import "fmt"

// Internal structures
type (
	state struct{}

	start    struct{ state }
	paused   struct{ state }
	running  struct{ state }
	finished struct{ state }
	killed   struct{ state }

	State interface {
		fmt.Stringer
		Valid() bool
		isState() // marker
	}
)

// Entry points
func Start() start       { return start{} }
func Paused() paused     { return paused{} }
func Running() running   { return running{} }
func Finished() finished { return finished{} }
func Killed() killed     { return killed{} }

// Stringer
func (start) String() string    { return "start" }
func (paused) String() string   { return "paused" }
func (running) String() string  { return "running" }
func (finished) String() string { return "finished" }
func (killed) String() string   { return "killed" }

// Closed for extension.
func (state) isState() {}

// Is this a valid state?
func (state) Valid() bool { return true }
