// file automatically generated by fsmgen at Tue, 04 Apr 2023 20:47:39 PDT
// modify or implement the following functions to customize the generated code
//
//go:generate
package main

import (
	"basic/blackhole/with_gv_error_state/no_subgraph/prof/prof"
)

func main() {

	// any state can be an initial state
	s := prof.Start()

	// example transition
	s.OnExit()

	// these can be chained too.

	// illegal state transitions are syntactically invalid, and cannot be invoked.
}
