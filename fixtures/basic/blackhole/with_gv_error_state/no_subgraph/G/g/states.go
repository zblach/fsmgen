// file automatically generated by fsmgen at Tue, 04 Apr 2023 20:47:39 PDT
// do not modify the following definitions
//
//vim:ro
//go:generate
package g

import "fmt"

// Internal structures
type (
	state struct{}

	a0    struct{ state }
	a1    struct{ state }
	a2    struct{ state }
	a3    struct{ state }
	b0    struct{ state }
	b1    struct{ state }
	b2    struct{ state }
	b3    struct{ state }
	start struct{ state }
	end   struct{ state }

	State interface {
		fmt.Stringer
		Valid() bool
		isState() // marker
	}
)

// Entry points
func A0() a0       { return a0{} }
func A1() a1       { return a1{} }
func A2() a2       { return a2{} }
func A3() a3       { return a3{} }
func B0() b0       { return b0{} }
func B1() b1       { return b1{} }
func B2() b2       { return b2{} }
func B3() b3       { return b3{} }
func Start() start { return start{} }
func End() end     { return end{} }

// Stringer
func (a0) String() string    { return "a0" }
func (a1) String() string    { return "a1" }
func (a2) String() string    { return "a2" }
func (a3) String() string    { return "a3" }
func (b0) String() string    { return "b0" }
func (b1) String() string    { return "b1" }
func (b2) String() string    { return "b2" }
func (b3) String() string    { return "b3" }
func (start) String() string { return "start" }
func (end) String() string   { return "end" }

// Closed for extension.
func (state) isState() {}

// Is this a valid state?
func (state) Valid() bool { return true }
