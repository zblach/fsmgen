// file automatically generated by fsmgen at Tue, 04 Apr 2023 20:47:39 PDT
// do not modify the following definitions
//
//vim:ro
//go:generate
package engine

// state transition interfaces
type (
	maybeStart interface {
		State

		Paused() maybePaused
		Running() maybeRunning
	}
	maybePaused interface {
		State

		Finished() maybeFinished
		Killed() maybeKilled
		Running() maybeRunning
	}
	maybeRunning interface {
		State

		Finished() maybeFinished
		Paused() maybePaused
		Running() maybeRunning
	}
	maybeFinished interface {
		State

		Killed() maybeKilled
	}
	maybeKilled interface {
		State
	}
)
