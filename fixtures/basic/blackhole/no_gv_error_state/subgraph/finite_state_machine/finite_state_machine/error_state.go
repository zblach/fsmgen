// file automatically generated by fsmgen at Tue, 04 Apr 2023 20:47:39 PDT
// do not modify the following definitions
//
//vim:ro
//go:generate
package finite_state_machine

import (
	"errors"
	"fmt"
)

type (
	_err struct {
		error
		from, to State
	}

	Error interface {
		State
		error

		LR0() maybeLR0
		LR3() maybeLR3
		LR4() maybeLR4
		LR8() maybeLR8
		LR2() maybeLR2
		LR1() maybeLR1
		LR6() maybeLR6
		LR5() maybeLR5
		LR7() maybeLR7
	}
)

// make _err adhere to state interface
func (_err) isState()    {}
func (_err) Valid() bool { return false }

// Error wrapping
func (e _err) Is(target error) bool {
	return e.error == target || errors.Is(e.error, target)
}

// Blackhole Transitions

func (e _err) LR0() maybeLR0 { return e }
func (e _err) LR3() maybeLR3 { return e }
func (e _err) LR4() maybeLR4 { return e }
func (e _err) LR8() maybeLR8 { return e }
func (e _err) LR2() maybeLR2 { return e }
func (e _err) LR1() maybeLR1 { return e }
func (e _err) LR6() maybeLR6 { return e }
func (e _err) LR5() maybeLR5 { return e }
func (e _err) LR7() maybeLR7 { return e }

func (e _err) String() string {
	return fmt.Sprintf("%s: %s → %s", e.error, e.from, e.to)
}
